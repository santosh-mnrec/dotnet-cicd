﻿using DotnetCore.Gitlab.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DotnetCore.Gitlab.Test
{
    public class LogServiceTest
    {
        private readonly Mock<ILogService> _logService;
        public LogServiceTest()
        {
            _logService = new Mock<ILogService>();
        }

        [Fact]
        public void Should_Call_Log()
        {

            _logService.Setup(x => x.Log()).Verifiable();
            var logservice = new LogService();
            logservice.Log();
            _logService.Verify(x => x.Log(), Times.Never);
        }
    }
}
