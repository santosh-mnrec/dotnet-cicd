FROM mcr.microsoft.com/dotnet/core/sdk:3.1 
WORKDIR /app 

# Copy everything and build
COPY . ./
# You can point directly at your csproj file, specially if it's on a sub-folder
# On some .Net Core projects (MVC, usually) if you don't add --runtime linux-x64, it won't publish. See note (1)
RUN dotnet publish -c Release -o out *.sln --runtime linux-x64 

# # Build runtime image
# FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
# WORKDIR /app
# COPY --from=build-env /app/out . 

# Change the name of the app here if needed (TestApp)
# CMD ASPNETCORE_URLS=http://*:$PORT dotnet DotnetCore.Gitlab.dll
ENTRYPOINT ["dotnet", "DotnetCore.Gitlab.dll"]
