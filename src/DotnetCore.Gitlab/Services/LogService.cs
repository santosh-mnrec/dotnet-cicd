﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetCore.Gitlab.Services
{
    public class LogService : ILogService
    {
        public void Log()
        {
            Console.WriteLine("Hello world");
        }
        public void Log(string error){
            System.Console.WriteLine("Error");
        }
        public string LogException(Exception exception,string msg){
            System.Console.WriteLine($"{exception} {msg}");
            return "done";
        }
    }
}
